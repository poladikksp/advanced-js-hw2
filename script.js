//? Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію `try...catch`.
//* будь-яке місце в коді де може з'явитись помилка
//* Перевірка отриманого об'єкту в форматі JSON, щоб код не ломався при отриманні файлу з помилкою.
//* слід використовувати при отриманні данних ззовні, коли ми не можемо знати що буде передано в наш код, щоб при передачі якихось некоректних данних обробити помилку та виконати код що залишився після помилки

"use strict";

const books = [
  {
    name: "First on the Moon",
    author: "Neil Armstrong",
    price: 300,
    year: 1970,
  },
  {
    author: "Люсі Фолі",
    price: 70,
  },
  {
    name: "Таємничий Острів",
    author: "Жюль Верн",
    price: 100,
  },
  {
    name: "Джонатан Стрейндж і м-р Норрелл",
    author: "Сюзанна Кларк",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    name: "Неономікон",
    author: "Алан Мур",
    price: 70,
  },
  {
    name: "Рухомі картинки",
    author: "Террі Пратчетт",
    price: 40,
    popularity: 60,
  },
  {
    name: "Коти в мистецтві",
    author: "Анґус Гайленд",
  },
  {
    name: "Black Holes and Time Warps",
    author: "Kip Thorne",
  },
];

function bookAdder(rootParent, listToAppend, bookObj) {
  let bookInfo = "";
  for (const prop in bookObj) {
    bookInfo += `${prop}: ${bookObj[prop]} <br>`;
  }
  listToAppend.insertAdjacentHTML("beforeend", `<li>${bookInfo}</li>`);
  rootParent.append(listToAppend);
}

function bookChecker(booksArr) {
  const requiredProps = ["name", "author", "price"];
  let missingPropIndex;

  booksArr.forEach((book, index) => {
    try {
      const bookPassed = requiredProps.every((prop, index) => {
        missingPropIndex = index;
        return Object.keys(book).includes(prop);
      });

      if (bookPassed) {
        bookAdder(root, bookList, book);
      } else {
        throw new Error(`book at index ${index} ${JSON.stringify(book)} 
        does not have the "${requiredProps[missingPropIndex]}" property`);
      }
    } catch (error) {
      console.error(error.message);
    }
  });
}

const bookList = document.createElement("ul");
const root = document.querySelector("#root");
bookChecker(books);
